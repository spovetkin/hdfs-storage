package ru.spovetkin.demo.hdfsstorage;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.hdfs.web.WebHdfsFileSystem;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

@SpringBootApplication
public class HdfsStorageApplication {

    private String hdfsuri = "hdfs://172.30.155.179:9000";
    private String webhdfsuri = "webhdfs://172.30.155.179:9870/webhdfs/v1/";

    public static void main(String[] args) {
        SpringApplication.run(HdfsStorageApplication.class, args);
    }

    @PostConstruct
    private void postConstruct() throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", webhdfsuri);
        FileSystem fs = WebHdfsFileSystem.get(configuration);
        System.out.println("fs=" + fs);
        Path homeDirectory = fs.getHomeDirectory();
        System.out.println("homeDirectory=" + homeDirectory);

        Path newFilePath = new Path(homeDirectory.toUri().toString() + "/testfile.txt");
        createFile(fs, newFilePath, "Тестовый текст файла");

        System.out.println("files:");
        RemoteIterator<LocatedFileStatus> files = fs.listFiles(fs.getHomeDirectory(), true);
        while (files.hasNext()) {
            LocatedFileStatus file = files.next();
            System.out.println("file=" + file.getPath() + ", len=" + file.getLen());
            String text = readFile(fs, file.getPath());
            System.out.println("text=" + text);
        }

        fs.close();
    }

    private void createFile(FileSystem fs, Path newFilePath, String text) throws IOException {
        if (fs.exists(newFilePath)) {
            return;
        }

        OutputStream os = fs.create(newFilePath);
        BufferedWriter br = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8.name()));
        br.write(text);
        br.close();
    }

    private String readFile(FileSystem fs, Path filePath) throws IOException {
        FSDataInputStream is = fs.open(filePath);
        String text = IOUtils.toString(is, StandardCharsets.UTF_8.name());
        is.close();
        return text;
    }

}
